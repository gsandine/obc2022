# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# ## 2022 PSU Optimization Boot Camp code repository
#
# 1. Clone the repository:
#
# ```bash
# $ git clone https://gitlab.com/gsandine/obc2022.git
# ```
#
# 2. Initialize and activate virtualenv, install requirements, run JupyterLab
#
# ```bash
# $ cd obc2022
# $ python3 -m venv ~/.obc # requires python3-venv
# $ . ~/.obc/bin/activate
# $ pip install -r requirements.txt
# ...
# $ jupyter-lab
# ```
# You should now be able to open this notebook file.

# cd hw2

# %run hw2.py

res = hw2()

from plots import *

plots(res=res, show=True, f_vals=True, df_vals=True, x_path=True)


