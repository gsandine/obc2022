## 2022 PSU Optimization Boot Camp code repository

1. Clone the repository:

```bash
$ git clone https://gitlab.com/gsandine/obc2022.git
```

2. Initialize and activate virtualenv, install requirements, run JupyterLab

```bash
$ cd obc2022
$ python3 -m venv ~/.obc # requires python3-venv
$ . ~/.obc/bin/activate
$ pip install -r requirements.txt
...
$ jupyter-lab
```
You should now be able to open and run these notebook files.
