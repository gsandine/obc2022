function [x,fval,gnorm,niter,nfeval,ngrad] = hw2mth464564(A, x0, alpha0, tol, imethod, maxit)

%input: 
% A coefficient for the Rosenbrock function
% x0 initial guess vector
% alpha0 initial step size (length)
% tol tolerance criteria for the gradient norm to stop the main iteration
% imethod parameter used to select the optimization method
% imethod = 1 implements Newton
% imethod = 2 implements steepest descent
% for all other integer values imethod implements scaled steepest descent
% maxit the maximum number of iterations allowed (set a large number e.g.,
% 100000)

%output
% x the approximate solution to the optimization 
% fval the value of the cost function at x, that is f(x)
% gnorm the norm of the gradient at x
%REMARK: this script stores the whole sequence of iterates, that is 
% x = [x0, x1, ..., xlast], same for fval and gnorm. 
% niter the number of iterations required to reach the optimal solution
% nfeval the number of function evaluations
% ngrad the number of gradient evaluations

%local parameters for line backtracking
c = 0.01; factor = 0.5; nmax = 30; 

iflag = 0;
niter = 0;
nfeval = 0; ngrad = 0;

n = length(x0);
x0 = x0(:); %column format
f0 = fcost(x0,A); nfeval = nfeval+1;
g0 = fgrad(x0,A); ngrad = ngrad + 1;
gnormk = norm(g0,2);


%trajectory storage 
x = x0;
fval = f0;
gnorm = gnormk;

iplot = 0;
if iplot
   doplot(A)
   plot(x0(1),x0(2),'ro','Linewidth',10) 
   pause(1)
end

while gnormk > tol 
    if imethod == 1 % Newton's method
        D = fhess(x0,A); 
        p0 = -D\g0;
    elseif imethod == 2 % steepest descent method
        p0 = -g0;      
    else % scaled steepest descent
        d = diag(fhess(x0,A));
        d=d(:);
        for j=1:n
            if d(j) <= 0
                d(j) = 1;
            end
        end
        p0 = -g0./d;
    end 
    
    [alpha,nfeval,f1,x1,iflag] = linebacktrack(alpha0,c,factor,nmax,x0,f0,p0,g0,A,nfeval);
    %alpha = alpha0; %implements a constant step size alpha0
    if iflag ~= 0 
        break;
    else    
        
 
        %x0 = x0 + alpha*p0;
        x0 = x1;
        if iplot
           plot(x0(1),x0(2),'go','Linewidth',10) 
           pause(0.1)
        end
        niter = niter+1;        
        %f0 = fcost(x0,A); nfeval = nfeval+1;
        f0 = f1;
        g0 = fgrad(x0,A); ngrad = ngrad + 1;
        gnormk = norm(g0,2);
        % store trajectory 
        x = [x x0]; fval = [fval; f0]; gnorm = [gnorm; gnormk]; 
        
        if niter == maxit 
           fprintf('Max number of iterations reached: %i',niter) 
           break; 
        end 
        
    end
end


nfeval
ngrad
niter


end


function [alpha,nfeval,f1,x1,iflag] = linebacktrack(alpha0,c,factor,nmax,x0,f0,p0,g0,A,nfeval)

alpha = alpha0;
sd = (g0'*p0)*c;
x1 = x0 + alpha*p0; 
f1 = fcost(x1,A); nfeval = nfeval+1;
iter = 1; iflag = 0;
%plot(x1(1),x1(2),'bo','Linewidth',5)

%pause(1)
while f1 > f0 + alpha*sd 
    alpha = alpha*factor;
    x1 = x0 + alpha*p0;
    f1 = fcost(x1,A);nfeval = nfeval+1;
    %fprintf('new step = %f\n',alpha); 
 %   figure(1)
 %   plot(x1(1),x1(2),'bo','Linewidth',5)
 %   pause(0.5)

    iter = iter+1;
    if iter > nmax 
        fprintf('Max iterations in line search reached %i', iter); 
        iflag = 1;
        break;
    end 
    
end
end

function f = fcost(x,A) 

f = A*(x(2) - x(1)^2)^2 + (1-x(1))^2;
end

function g = fgrad(x,A)

g = zeros(2,1);
g(1) = -4*A*(x(2)- x(1)^2)*x(1) -2*(1-x(1)); 
g(2) = 2*A*(x(2) - x(1)^2);

end

function H = fhess(x,A)

H = zeros(2,2);

H(1,1) = 12*A*x(1)^2 -4*A*x(2) + 2;
H(1,2) = -4*A*x(1); 
H(2,1) = H(1,2);
H(2,2) = 2*A;

end



function doplot(A)
[x,y] = meshgrid(-2:.1:2, -2:.1:2);
%[x,y] = meshgrid(-5:.1:5, -5.:.1:5);
f1 = (y - x.^2); 
f2 = 1-x ; 
f = A*f1.^2 + f2.^2;
figure(1)
hold on
[c,h]= contour(x,y,f,0:1:20);
clabel(c,h)
%axis square
hold on
plot(1,1,'gs','Linewidth',10)
title(strcat('Rosenbrock function: f(x,y) =', num2str(A), ... 
    '(y-x^2)^2 + (1-x)^2. Optimal point (1,1)'),'fontsize',16)

end

    


    
