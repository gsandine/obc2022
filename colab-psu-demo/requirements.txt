# to turn Python .py program into Jupyter Notebook .ipynb
jupytext

# for running code and plotting
numpy
matplotlib

# for JupyterLab in your computer
wheel
jupyterlab
