#!/bin/bash -l

#SBATCH --job-name coeus_hw2
#SBATCH --nodes 1
#SBATCH --ntasks 1
#SBATCH --cpus-per-task 1
#SBATCH --mem 5M
#SBATCH --partition short
#SBATCH --time-min 00:01:00

#SBATCH --output coeus_hw2_%j.out
#SBATCH --error coeus_hw2_%j.err
#SBATCH --mail-type=begin
#SBATCH --mail-type=end 
#SBATCH --mail-type=fail 
#SBATCH --mail-user=gsandine@pdx.edu

# either use the full path to your venv's python3
~/.obc/bin/python3 colab-demo.py

# or activate the venv and use bare python3 command
#. ~/.obc/bin/activate
#python3 colab-demo.py
