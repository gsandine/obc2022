## Setup

Do these things before any of the demo items below. Do these in your computer
for the Colab demo, in a Coeus login node for the Coeus demo, and in one
of the standalone computer servers for the stanadlone compute demo.

1. Clone the repository and enter the code directory

```bash
$ git clone https://gitlab.com/gsandine/obc2022.git
Cloning into 'obc2022'...
remote: Enumerating objects: 71, done.
remote: Counting objects: 100% (14/14), done.
remote: Compressing objects: 100% (14/14), done.
remote: Total 71 (delta 5), reused 0 (delta 0), pack-reused 57
Unpacking objects: 100% (71/71), done.
$ cd obc2022/colab-psu-demo
$  
```
2. Initialize and activate a Python virtual environment

```bash
$ python3 -m venv ~/.obc # requires python3-venv in Ubuntu
$ . ~/.obc/bin/activate
$  
```

## Google Colab demo

1. Install jupytext

```bash
$ pip3 install jupytext
...
$ 
```

2. Create Jupyter Notebook .ipynb file from Python .py program

```bash
$ jupytext --to ipynb colab-demo.py
[jupytext] Reading colab-demo.py in format py
[jupytext] Writing colab-demo.ipynb
$  
```

You should now be able to import colab-demo.ipynb into Google Colab and run it.


## PSU Coeus demo

1. Install NumPy and Matplotlib into your activated Python virtual environment

```bash
$ pip3 install numpy matplotlib
...
$  
```

2. Submit the job to the Slurm scheduler (OK to deactivate venv); check status

```bash
$ sbatch coeus_hw2.sh
Submitted batch job X
$ squeue -u gsandine
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
                 X     short coeus_hw gsandine  R       0:05      1 compute127
$  
```

Plots will be in `.png` files in the directory from which the job was launched.


## PSU standalone Linux compute server demo

1. Install NumPy and Matplotlib into your activated Python virtual environment

```bash
$ pip3 install numpy matplotlib
...
$  
```

2. Run the demo Python program (in activated Python virtual environment)

```bash
$ python3 colab-demo.py
$  
```

Plots will be in `.png` files in the directory from which the job was launched.
