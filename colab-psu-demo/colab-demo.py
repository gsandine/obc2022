# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# Define optimization functions

# + tags=[]
# optimization functions
import numpy as np

def const_stepsize(args, **kwargs):
    """ Constant step size function

        Inputs
        args   : Dictionary of arguments, requires:
                 alpha : Constant step size value
                 x_k   : Current iterate; for compatibility - unused
                 f_k   : Current value f_k; for compatibility - unused
        kwargs : Optional keyword args:
                 For compatibility - unused

        Outputs
        alpha  : Constant step size
        x_k    : Current iterate since we don't know what the next will be
        f_k    : Cost function value at current iterate
    """
    # return constant step size
    return args['alpha'], args['x_k'], args['f_k']


def backtrack_stepsize(args, **kwargs):
    """ Backtracking step size function - for a given iterate x_k
        and direction p_k, it finds and returns a step size such
        that the first Wolfe condition W1 is satisfied. It also
        returns the next iterate and the value of the cost function
        at the next iterate since they are both computed while
        searching for the next step size.

        Inputs
        args   : Dictionary of arguments, requires:
                 f     : Class instance for cost function with eval()
                         method for evaluating f at a point and with
                         grad() method for calculating the gradient
                         at a point
                 alpha : Initial step size
                 rho   : Backtracking factor for scaling alpha if the
                         Wolfe condition W1 is not satisfied
                 c     : Constant for the first Wolfe condition W1
                 x_k   : Current iterate
                 f_k   : Cost function value at current iterate
                 p_k   : Search direction
        kwargs : Optional keyword args:
                 df_k  : Holds df_k if it is already known
   
        Outputs:
        alpha        : Step size from x_k in direction p_k such that
                       the first Wolfe condition W1 is satisfied
        x_k1         : Next iterate
        f_k1         : Cost function value at next iterate
    """
    # unpack arguments
    f = args['f']
    alpha = args['alpha']
    rho = args['rho']
    c = args['c']
    x_k = args['x_k']
    f_k = args['f_k']
    p_k = args['p_k']

    # obtain df(x_k) for checking first Wolfe condition W1
    if 'df_k' in kwargs.keys():
        df_k = kwargs['df_k']
    else:
        df_k = f.grad(x_k)

    # scale step size by rho until first Wolfe condition W1 is satisfied
    x_k1 = x_k + alpha * p_k
    f_k1 = f.eval(x_k1)
    c_p_df = c * p_k @ df_k
    while f_k1 > (f_k + alpha * c_p_df):
        alpha = rho * alpha
        x_k1 = x_k + alpha * p_k
        f_k1 = f.eval(x_k1)

    # return step size
    return alpha, x_k1, f_k1


def steepest_dir(args, **kwargs):
    """ Function for calculating the steepest descent search direction

        Inputs
        args   : Dictionary of arguments, requires:
                 f   : Class instance for cost function with grad() method
                       for calculating the gradient at a point
                 x_k : Point at which to calculate the gradient
        kwargs : Optional keyword args:
                 df_k  : Holds df_k if it is already known

        Outputs
        Steepest descent search direction -f(x_k)
    """
    # return steepest descent direction at x_k
    if 'df_k' in kwargs.keys():
        return -kwargs['df_k']
    return -args['f'].grad(args['x_k'])


def newton_dir(args, **kwargs):
    """ Function for calculating the Newton's method search direction

        Inputs
        args   : Dictionary of arguments, requires:
                 f   : Class instance for cost function with grad() and
                       hess() methods for calculating the gradient and
                       the Hessian at a point
                 x_k : Point at which to calculate the gradient and hessian
        kwargs : Optional keyword args:
                 df_k  : Holds df_k if it is already known

        Outputs
        Newton's method search direction -(d^2f(x_k))^{-1}(df(x_k)) as the
        solution to the linear system -d^2f(x_k)x=df(x_k)
    """
    # return Newton's method direction at x_k
    if 'df_k' in kwargs.keys():
        return -np.linalg.solve(args['f'].hess(args['x_k']),kwargs['df_k'])
    return -np.linalg.solve(args['f'].hess(args['x_k']),args['f'].grad(args['x_k']))


def FR_dir(args, **kwargs):
    """ Function for finding the Fletcher-Reeves nonlinear CG direction

        Inputs
        args   : Dictionary of arguments, requires:
                 x_kf  : Class instance for cost function with grad() method
                       for calculating the gradient at a point
                 x_k : Point at which to calculate the gradient
        kwargs : Optional keyword args:
                 df_k  : Holds df_k if it is already known
    """
    # populate or compute grad(x_k) at current iteration x_k)
    if 'df_k' in kwargs.keys():
        df_k = kwargs['df_k']
    else:
        df_k = args['f'].grad(args['x_k'])
    # The previous search direction and the norm of the previous gradient
    # are needed for the next iteration. Those will not be defined the first
    # time the function is invoked, so return p_k = -grad(x_k) and store
    # p_k and ||grad(x_k)|| to be used for the next iteration.
    if not 'p_k' in args.keys():
        g_k = df_k
        p_k = -g_k
        args['p_k'] = p_k
        args['normg'] = np.linalg.norm(g_k)
        return p_k
    # find Fletcher-Reeves nonlinear CG direction
    g_k = df_k
    normg = np.linalg.norm(g_k)
    beta_k = (normg / args['normg'])**2
    p_k = -g_k + beta_k*args['p_k']
    # return p_k = -grad(x_k) if calculated p_k is not a descent direction
    if (p_k @ g_k) >= 0:
        p_k = -df_k
        print('*** restart at iteration %d' % kwargs['niter'])
    # store direction and norm of gradient for next iteration
    args['p_k'] = p_k
    args['normg'] = normg
    # return new direction
    return p_k

def FR_dir_dot(args, **kwargs):
    """ Function for finding the Fletcher-Reeves nonlinear CG direction
        using g@g for \|g\|^2 instead of norm(g)^2

        Inputs
        args   : Dictionary of argumentsf, requires:
                 x_kf  : Class instance for cost function with grad() method
                       for calculating the gradient at a point
                 x_k : Point at which to calculate the gradient
        kwargs : Optional keyword args:
                 df_k  : Holds df_k if it is already known
    """
    # populate or compute grad(x_k) at current iteration x_k)
    if 'df_k' in kwargs.keys():
        df_k = kwargs['df_k']
    else:
        df_k = args['f'].grad(args['x_k'])
    # The previous search direction and the norm of the previous gradient
    # are needed for the next iteration. Those will not be defined the first
    # time the function is invoked, so return p_k = -grad(x_k) and store
    # p_k and ||grad(x_k)|| to be used for the next iteration.
    if not 'p_k' in args.keys():
        g_k = df_k
        p_k = -g_k
        args['p_k'] = p_k
        args['g_dot_g'] = g_k @ g_k
        return p_k
    # find Fletcher-Reeves nonlinear CG direction
    g_k = df_k
    g_dot_g = g_k @ g_k
    beta_k = g_dot_g / args['g_dot_g']
    p_k = -g_k + beta_k*args['p_k']
    # return p_k = -grad(x_k) if calculated p_k is not a descent direction
    if (p_k @ g_k) >= 0:
        p_k = -df_k
        print('*** restart at iteration %d' % kwargs['niter'])
    # store direction and norm of gradient for next iteration
    args['p_k'] = p_k
    args['g_dot_g'] = g_dot_g
    # return new direction
    return p_k


def graditer(x_k, f, stepdir, stepdir_args, stepsize, stepsize_args, \
        eps=1.0e-5, maxiter=1.0e+6):
    """ graditer(): Iterative line search optimization algorithm

        This solves an optimization problem where at each point one
        chooses a direction and a step size. It can use any function
        for choosing a step direction and any function for choosing a
        step size at a given point x_k.

        Inputs
        x_k           : Starting point
        f             : Class instance for cost function. Requires methods
                        f.eval(x) returning f(x) and f.grad(x) returning
                        grad f(x)
        stepdir       : Function for computing a step direction at the current
                        iterate x_k
        stepdir_args  : Arguments required for step direction function
        stepsize      : Function for returning step size alpha at the current
                        iterate x_k and direction p_k
        stepsize_args : Arguments required for step size function
        eps           : Stopping criterion - stop if norm of gradient is
                        less than eps (optional - default 1.0e-5)
        maxiter       : Stopping criterion - stop if number of iterations
                        reaches maxiter (optional - default 1.0e+6)

        Outputs
        iters         : 2d array of iterates {x_k}
        f_vals        : Array of f values {f(x_k)}
        norm_df_vals  : Array of ||df|| values {||df(x_k)||}
    """
    # initialize niter (# iterations) and iters list (stores iterate data)
    # and store initial iterate, f_k, and |df_k| vals
    niter = 0
    iters = np.array([x_k])
    f_k = f.eval(x_k)
    f_vals = np.array([f_k])
    df_k = f.grad(x_k)
    norm_df_k = np.linalg.norm(df_k)
    norm_df_vals = np.array([norm_df_k])

    # begin loop
    while niter < maxiter:
        stepdir_args['x_k'] = x_k
        p_k = stepdir(stepdir_args, df_k=df_k, niter=niter)
        # set x_k and direction p_k in stepsize_args and get next step size
        stepsize_args['x_k'] = x_k
        stepsize_args['f_k'] = f_k
        stepsize_args['p_k'] = p_k
        alpha, _x_k1, _f_k1 = stepsize(stepsize_args, df_k=df_k)
        # calculate next iterate
        if (_x_k1 != x_k).all():
            x_k = _x_k1
        else:
            x_k = x_k + alpha * p_k
        # update values for next loop and store current state
        if _f_k1 != f_k:
            f_k = _f_k1
        else:
            f_k = f.eval(x_k)
        df_k = f.grad(x_k)
        norm_df_k = np.linalg.norm(df_k)
        iters = np.append(iters, [x_k], axis=0)
        f_vals = np.append(f_vals, f_k)
        norm_df_vals = np.append(norm_df_vals, norm_df_k)
        # increment number of iterations
        niter += 1
        # stop if |df(x_k)| < eps
        if norm_df_k < eps:
            break
    # return results
    return iters,f_vals,norm_df_vals

def cgiter(x_k, A, b, eps=1.0e-5, maxiters=1.0e+4):
    """ Linear CG function for solving Ax=b

        Required Inputs
        x_k      : Starting point
        A        : Matrix A in Ax=b
        b        : Solution b in Ax=b

        Optional Inputs
        eps      : Stop when ||Ax-b|| < eps (default 1.0e-5)
        maxiters : Stop if iterations exceed maxiter (default 1.0e+4)

        Outputs
        iters    : Array of iterates - last one is the solution
        r_k_vals : Array of residuals for each iterate
    """

    # initialize niter (# iterations), iters (iterats), r_k_vals (residuals)
    niter = 0
    iters = np.array([x_k])
    r_k = A@x_k - b
    r_k_vals = np.array([r_k])
    # calculate first residual norm and first CG direction
    norm_r_k = np.linalg.norm(r_k)
    p_k = -r_k
    # begin CG loop
    while (norm_r_k > eps) and (niter < maxiters):
        # generate next iterate x_k and residual r_k
        Ap_k = A@p_k
        alpha_k = (norm_r_k**2)/(p_k @ Ap_k)
        x_k1 = x_k + alpha_k * p_k
        r_k1 = r_k + alpha_k * Ap_k
        # save x_k and r_k
        iters = np.append(iters, [x_k1], axis=0)
        r_k_vals = np.append(r_k_vals, r_k1)
        # get next search direction p_k
        norm_r_k1 = np.linalg.norm(r_k1)
        beta_k1 = (norm_r_k1**2)/(norm_r_k**2)
        p_k1 = -r_k1 + beta_k1 * p_k
        # store new iterate, direction, residual, residual norm for next loop
        x_k = x_k1
        p_k = p_k1
        r_k = r_k1
        norm_r_k = norm_r_k1
        niter += 1

    # return iterates and residuals
    return iters,r_k_vals


# -

# Define plotting functions

# + tags=[]
# plotting functions
import numpy as np
import matplotlib.pyplot as plt

def plot_f_vals(res, show):
    for A in res.keys():
        # steepest descent cost function
        plt.clf()
        plt.plot(res[A]['s']['f_vals'], linewidth='2')
        plt.yscale('log')
        if A==1:
            plt.ylim((1.0e-13,1.0e+1))
        else:
            plt.ylim((1.e-12,1.e+2))
        plt.xlabel('k')
        plt.ylabel('log f_k')
        plt.title('Cost Function Evolution for Steepest Descent (A=%d)' % A)
        plt.tight_layout()
        if show:
            plt.show()
        else:
            plt.savefig('f_vals-steepest-A=%03d.png' % A)

        # Newton's method cost function
        plt.clf()    
        plt.plot(res[A]['n']['f_vals'], lw=2)
        plt.yscale('log')
        if A==1:
            plt.ylim((1.0e-13,1.0e+1))
        else:
            plt.xlim((-1,22))
            plt.ylim((1.e-12,1.e+2))
        plt.xlabel('k')
        plt.ylabel('log f_k')
        plt.title('Cost Function Evolution for Newton\'s Method (A=%d)' % A)
        plt.tight_layout()
        if show:
            plt.show()
        else:
            plt.savefig('f_vals-newton-A=%03d.png' % A)

def plot_df_vals(res, show):
    for A in res.keys():
        # full plot - norm of gradient for S
        plt.clf()
        plt.plot(res[A]['s']['norm_df_vals'], lw=1, c='b', label='log|grad f_k|')
        stop = np.ones(len(res[A]['s']['norm_df_vals']))*1.0e-3
        plt.plot(stop, lw=1.25, c='r', alpha=0.5, ls='--', label=r'stop')
        plt.yscale('log')
        if A==1:
            plt.ylim((1.e-6,10.))
        else:
            plt.ylim((1.e-5,1.e+3))
        plt.xlabel('k')
        plt.ylabel('log|grad f_k|')
        plt.legend()
        plt.title('Norm of Gradient for Steepest Descent (A=%d)' % A)
        plt.tight_layout()
        if show:
            plt.show()
        else:
            plt.savefig('norm_df_vals-steepest-A=%03d.png' % A)

        # full plot - norm of gradient for N
        plt.clf()
        plt.plot(res[A]['n']['norm_df_vals'], lw=2, c='b', label='log|grad f_k|')
        stop = np.ones(len(res[A]['n']['norm_df_vals']))*1.0e-3
        plt.plot(stop, lw=1.25, c='r', alpha=0.5, ls='--', label='stop')
        plt.yscale('log')
        if A==1:
            plt.ylim((1.e-6,10.))
        else:
            plt.xlim((-1,22))
            plt.ylim((1.e-5,1.e+3))
        plt.xlabel('k')
        plt.ylabel('log|grad f_k|')
        plt.legend()
        plt.title('Norm of Gradient for Newton\'s Method (A=%d)' % A)
        plt.tight_layout()
        if show:
            plt.show()
        else:
            plt.savefig('norm_df_vals-newton-A=%03d.png' % A)

        if A == 100:
            # 60, 196, ~0.0737, ~0.0609
            # initial iterates - norm of gradient for S, only needed for A=100
            plt.clf()
            stop = 201
            plt.plot(res[A]['s']['norm_df_vals'][:stop], lw=1, c='b', label='log|grad f_k|')
            stop = np.ones(len(res[A]['s']['norm_df_vals'][:stop]))*1.0e-3
            plt.plot(stop, lw=1.25, c='r', alpha=0.5, ls='--', label='stop')
            plt.yscale('log')
            plt.ylim((0.5*10**(-3),1.e+3))
            plt.xlabel('k')
            plt.ylabel('log|grad f_k|')
            plt.legend()
            plt.title('Norm of Gradient for Steepest Descent (A=%d)' % A)
            plt.tight_layout()
            if show:
                plt.show()
            else:
                plt.savefig('norm_df_vals-steepest-A=%03d-start.png' % A)

        if A == 100:
            # final iterates - norm of gradient for $S, only needed for A=100
            plt.clf()
            start = -201
            niters = len(res[A]['s']['iters'])-1
            plt.plot(res[A]['s']['norm_df_vals'][start:], lw=1, c='b', label='log|grad f_k|')
            stop = np.ones(len(res[A]['s']['norm_df_vals'][start:]))*1.0e-3
            plt.plot(stop, lw=1.25, c='r', alpha=0.5, ls='--', label='stop')
            plt.yscale('log')
            k = ['%d'%n for n in np.arange(niters+(start+1),niters+1,25)]
            plt.xticks(range(0,-start,25),k)
            plt.ylim((0.5*10**(-3),1.e-2))
            plt.xlabel('k')
            plt.ylabel('log|grad f_k|')
            plt.legend()
            plt.title('Norm of Gradient for Steepest Descent (A=%d)' % A)
            plt.tight_layout()
            if show:
                plt.show()
            else:
                plt.savefig('norm_df_vals-steepest-A=%03d-end.png' % A)

def plot_x_path_z(res, show, zoom=0):
    # ran this manually adjusting boundaries for steepest descent with
    # zoom=-151 to show the end and zoom=30 to show the beginning
    for A in res.keys():
        for m in 'n','s':
            if zoom <= 0:
                iters = res[A][m]['iters'][zoom:]
                flstr = 'Last'; flnum = -zoom-1
            else:
                iters = res[A][m]['iters'][1:zoom+1]
                flstr = 'First'; flnum = zoom
            plt.clf()
            s = 5.
            if m == 's':
                s=0.5; s=5.
            #plt.scatter(*zip(*iters),s=s,c=np.arange(len(iters))/(len(iters)-1),cmap='seismic')
            dotcolor = 'k'
            if zoom < 0:
                dotcolor = 'r'
            elif zoom > 0:
                dotcolor = 'b'
            plt.scatter([1.],[1.],marker='+',color=dotcolor,s=20.)
            xmax=np.max(iters[:,0])+1.0e-5
            xmin=np.min(iters[:,0])-1.0e-5
            ymax=np.max(iters[:,1])+1.0e-5
            ymin=np.min(iters[:,1])-1.0e-5
            if zoom == 0:
                xmax = np.round(np.max([xmax+0.1,0.1]),1)
                xmin = np.round(np.min([xmin-0.1,-0.1]),1)
                ymax = np.round(np.max([ymax+0.1,0.1]),1)
                ymin = np.round(np.min([ymin-0.1,-0.1]),1)
                plt.scatter([iters[0][0]],[iters[0][1]],color='k',s=10.)
            dx=5.*(xmax-xmin)/1000.
            dy=5.*(ymax-ymin)/1000.
            plt.xlim((xmin,xmax))
            plt.ylim((ymin,ymax))
            plt.xlabel('x_1')
            plt.ylabel('x_2')
            method = 'Newton\'s Method'; smethod = 'newton'
            if m == 's':
                method = '%s %d Steepest Descent' % (flstr,flnum); smethod = 'steepest'
            plt.title('%s Iterations (A=%d)' % (method,A))
            plt.plot(np.arange(xmin,xmax+0.9e-4*dx,dx),np.zeros(len(np.arange(xmin,xmax+0.9e-4*dx,dx))),c='k',lw=1.5,alpha=0.7)
            plt.plot(np.zeros(len(np.arange(ymin,ymax+0.9e-4*dy,dy))),np.arange(ymin,ymax+0.9e-4*dy,dy),c='k',lw=1.5,alpha=0.7)
            plt.grid()
            cplot(xmin,xmax,ymin,ymax,dx,dy,A)
            plt.tight_layout()
            if show:
                plt.show()
            else:
                plt.savefig('x_path-%s-A=%03d-%s%03d.png' % (smethod,A,flstr,flnum))

def plot_x_path(res, show, zoom=0):
    for A in res.keys():
        for m in 'n','s':
            iters = res[A][m]['iters']
            plt.clf()
            s = 5.
            if m == 's': s=0.5
            plt.scatter(*zip(*iters),s=s,c=np.arange(len(iters))/(len(iters)-1),cmap='seismic')
            plt.scatter([1.],[1.],marker='+',color='r',s=30.)
            plt.scatter([iters[0][0]],[iters[0][1]],color='k',s=20.)
            xmax=np.round(np.max([np.max(res[A][m]['iters'][:,0]),0.])+0.1,1)
            xmin=np.round(np.min([np.min(res[A][m]['iters'][:,0]),0.])-0.1,1)
            ymax=np.round(np.max([np.max(res[A][m]['iters'][:,1]),0.])+0.1,1)
            ymin=np.round(np.min([np.min(res[A][m]['iters'][:,1]),0.])-0.1,1)
            dx=0.005;dy=0.005
            plt.xlim((xmin,xmax))
            plt.ylim((ymin,ymax))
            plt.xlabel('x_1')
            plt.ylabel('x_2')
            method = 'Newton\'s Method'; smethod = 'newton'
            if m == 's':
                method = 'Steepest Descent'; smethod = 'steepest'
            plt.title('%s Iterations (A=%d)' % (method,A))
            plt.plot(np.arange(xmin,xmax+0.9*dx,dx),np.zeros(len(np.arange(xmin,xmax+0.9*dx,dx))),c='k',lw=1.5,alpha=0.7)
            plt.plot(np.zeros(len(np.arange(ymin,ymax+0.9*dy,dy))),np.arange(ymin,ymax+0.9*dy,dy),c='k',lw=1.5,alpha=0.7)
            plt.grid()
            cplot(xmin,xmax,ymin,ymax,dx,dy,A)
            plt.tight_layout()
            if show:
                plt.show()
            else:
                plt.savefig('x_path-%s-A=%03d.png' % (smethod,A))

def plots(**kwargs):
    res = kwargs['res']
    show = False
    if 'show' in kwargs.keys() and kwargs['show']:
        show = True

    if 'f_vals' in kwargs.keys():
        plot_f_vals(res, show)
    if 'df_vals' in kwargs.keys():
        plot_df_vals(res, show)
    if 'x_path' in kwargs.keys():
        plot_x_path(res, show)

def cplot(xmin,xmax,ymin,ymax,dx,dy,A):
    # requires rosen() class from hw2.py
    f=rosen(A=A)
    #xmax=2.; dx=0.005; ymax=xmax; dy=dx
    #x=np.arange(-xmax,xmax+dx,dx);y=np.arange(-ymax,ymax+dy,dy)
    x=np.arange(xmin,xmax+0.9*dx,dx);y=np.arange(ymin,ymax+0.9*dy,dy)
    z=np.zeros((y.shape[0],x.shape[0]))
    for yi in range(y.shape[0]):
        for xj in range(x.shape[0]):
            xy = np.array([x[xj],y[yi]])
            #xy = np.array([y[-(yi+1)],x[xj]])
            z[yi,xj] = f.eval(xy)
    #plt.contour(x,y,z,np.arange(0.1,100.1,10.),colors='b',alpha=0.5)
    #cmax = z.max()/(0.2*A); numc = 40.
    #plt.contour(x,y,z,np.arange(0.1,cmax,cmax/numc),colors='r',alpha=0.2)
    plt.contour(x,y,z,100,colors='k',alpha=0.1)
    #plt.show()



# -

# Code using optimization functions above to solve the problem for A=1 and A=100

# + tags=[]
# homework 2 code
# import numerical libraries
import numpy as np

class rosen:
    """ Class for Rosenbrock test function with domain $^2:
        f(x) = A (x1 - x0^2)^2 + (1 - x0)^2

        Methods:
        eval(x) : returns f(x)
        grad(x) : returns the gradient df(x)
        hess(x) : returns the Hessian d2f(x)

        Attributes:
        f_evals     : number of f evaluations
        df_evals    : number of gradient evaluations
        d2f_evals   : number of Hessian evaluations
    """
    def eval(self, x):
        """ Class method that returns f(x)

            Inputs
            x    : Numpy 2d array at which to evaluate f

            Outputs
            f(x) : Value of the Rosenbrock function at x
        """
        # increment f_evals counter
        self.f_evals += 1
        # return f(x)
        return (self.A)*(x[1]-x[0]*x[0])**2+(1-x[0])**2
        #return (self.A)*np.power(x[1]-x[0]*x[0],2)+np.power(1-x[0],2)

    def grad(self, x):
        """ Class method that returns grad f(x)

            Inputs
            x     : Numpy 2d array at which to compute grad f

            Outputs
            df(x) : Gradient of the Rosenbrock function at x
        """
        # increment df_evals counter
        self.df_evals += 1
        # compute partial derivatives
        df_dx0 = -4*(self.A)*(x[1]-x[0]**2)*x[0]-2*(1-x[0])
        df_dx1 = 2*(self.A)*(x[1]-x[0]**2)
        # return df(x)
        return np.array([df_dx0,df_dx1])

    def hess(self, x):
        """ Class method that returns Hessian f(x)

            Inputs
            x      : Numpy 2d array at which to compute Hessian f

            Outputs
            d2f(x) : Hessian of the Rosenbrock function at x
        """
        # increment d2f_evals counter
        self.d2f_evals += 1
        # compute partial derivatives
        d2f_d2x1 = 12*(self.A)*(x[0])**2-4*(self.A)*x[1]+2
        d2f_d2x1x2 = -4*(self.A)*x[0]
        d2f_d2x2x1 = d2f_d2x1x2
        d2f_d2x2 = 2*(self.A)
        # return d2f(x)
        return np.array([[d2f_d2x1,d2f_d2x1x2],[d2f_d2x2x1,d2f_d2x2]])

    def __init__(self, A):
        """ Class initialization method - sets parameter for Rosenbrock
            function and initializes counters for the number of f, df,
            and d2f evaluations to 0

            Inputs
            A      : Parameter for Rosenbrock function A(x2-x1^2)^2+(1-x1)^2
        """
        # initialize parameter for Rosenbrock function
        self.A = A
        # initialize counters for the number of f, df, and d2f evaluations
        self.f_evals = 0
        self.df_evals = 0
        self.d2f_evals = 0

def hw2():
    # initialize x0
    x0 = np.array([-1.2,1.])

    # create dictionary for storing results
    res = {}

    # loop over A values for Rosenbrock function
    for A in 1., 100.:
        # initialize dictionaries for storing results for this A value
        res[A] = {}; s_res = {}; n_res = {}

        # initialize f with parameter A
        f = rosen(A)

        # initialize arguments for step direction function - it's the same for
        # this assignment for both methods (steepest descent and Newton's method)
        stepdir_args = {'f': f}

        # initialize arguments for step size functions - they're the same for this
        # assignment for both methods (steepest descent and Newton's method)
        stepsize_args = {'f': f, 'alpha': 1., 'rho': 0.5, 'c': 0.01}

        # invoke steepest descent algorithm with backtracking step size selection
        s_res['iters'],s_res['f_vals'],s_res['norm_df_vals'] = \
            graditer(x0, f, steepest_dir, stepdir_args, backtrack_stepsize, \
                stepsize_args, eps=1.0e-3)
        # store and reset function and gradient evaluation counters
        s_res['evals'] = (f.f_evals, f.df_evals)
        f.f_evals,f.df_evals = 0,0

        # invoke Newton's method algorithm with backtracking step size selection
        n_res['iters'],n_res['f_vals'],n_res['norm_df_vals'] = \
            graditer(x0, f, newton_dir, stepdir_args, backtrack_stepsize, \
                stepsize_args, eps=1.0e-3)
        # store function, gradient, and Hessian evaluation counters
        n_res['evals'] = (f.f_evals, f.df_evals, f.d2f_evals)

        # save results in res dictionary
        res[A] = {}; res[A]['s'] = s_res; res[A]['n'] = n_res

    # return results
    return res


# -

# Solve the problem and plot the results

# + tags=[]
if __name__ == '__main__':
    res = hw2()
    plots(res=res, show=False, f_vals=True, df_vals=True, x_path=True)
# -


