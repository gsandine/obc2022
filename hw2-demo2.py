# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# + [markdown] tags=[]
# ## 2022 PSU Optimization Boot Camp code repository
#
# 1. Clone the repository:
#
# ```bash
# $ git clone https://gitlab.com/gsandine/obc2022.git
# ```
#
# 2. Initialize and activate virtualenv, install requirements, run JupyterLab
#
# ```bash
# $ cd obc2022
# $ python3 -m venv ~/.obc # requires python3-venv
# $ . ~/.obc/bin/activate
# $ pip install -r requirements.txt
# ...
# $ jupyter-lab
# ```
# You should now be able to open this notebook file.

# +
# import numerical libraries
import numpy as np

# append to path to import local libraries
import sys, os
for _dir in ['lib','hw2']:
    sys.path.append(os.path.join(sys.path[0],_dir))

# import optimization functions from mth564 library
from mth564 import *

# import Rosenbrock function class 
from hw2 import rosen
# -

A = 1.
f = rosen(A)
x0 = np.array([-1.2,1.])

# + tags=[]
stepdir_args = {'f': f}
# -

stepsize_args = {'f': f, 'alpha': 1., 'rho': 0.5, 'c': 0.01}

s_iters, s_f_vals, s_df_vals = graditer(x0, f, steepest_dir, stepdir_args, backtrack_stepsize, stepsize_args, eps=1.0e-3)

len(s_iters),f.f_evals, f.df_evals, f.d2f_evals

s_iters[-1],s_f_vals[-1],s_df_vals[-1]


