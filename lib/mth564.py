#!/usr/bin/env python

import numpy as np


def const_stepsize(args, **kwargs):
    """ Constant step size function

        Inputs
        args   : Dictionary of arguments, requires:
                 alpha : Constant step size value
                 x_k   : Current iterate; for compatibility - unused
                 f_k   : Current value f_k; for compatibility - unused
        kwargs : Optional keyword args:
                 For compatibility - unused

        Outputs
        alpha  : Constant step size
        x_k    : Current iterate since we don't know what the next will be
        f_k    : Cost function value at current iterate
    """
    # return constant step size
    return args['alpha'], args['x_k'], args['f_k']


def backtrack_stepsize(args, **kwargs):
    """ Backtracking step size function - for a given iterate x_k
        and direction p_k, it finds and returns a step size such
        that the first Wolfe condition W1 is satisfied. It also
        returns the next iterate and the value of the cost function
        at the next iterate since they are both computed while
        searching for the next step size.

        Inputs
        args   : Dictionary of arguments, requires:
                 f     : Class instance for cost function with eval()
                         method for evaluating f at a point and with
                         grad() method for calculating the gradient
                         at a point
                 alpha : Initial step size
                 rho   : Backtracking factor for scaling alpha if the
                         Wolfe condition W1 is not satisfied
                 c     : Constant for the first Wolfe condition W1
                 x_k   : Current iterate
                 f_k   : Cost function value at current iterate
                 p_k   : Search direction
        kwargs : Optional keyword args:
                 df_k  : Holds df_k if it is already known
   
        Outputs:
        alpha        : Step size from x_k in direction p_k such that
                       the first Wolfe condition W1 is satisfied
        x_k1         : Next iterate
        f_k1         : Cost function value at next iterate
    """
    # unpack arguments
    f = args['f']
    alpha = args['alpha']
    rho = args['rho']
    c = args['c']
    x_k = args['x_k']
    f_k = args['f_k']
    p_k = args['p_k']

    # obtain df(x_k) for checking first Wolfe condition W1
    if 'df_k' in kwargs.keys():
        df_k = kwargs['df_k']
    else:
        df_k = f.grad(x_k)

    # scale step size by rho until first Wolfe condition W1 is satisfied
    x_k1 = x_k + alpha * p_k
    f_k1 = f.eval(x_k1)
    c_p_df = c * p_k @ df_k
    while f_k1 > (f_k + alpha * c_p_df):
        alpha = rho * alpha
        x_k1 = x_k + alpha * p_k
        f_k1 = f.eval(x_k1)

    # return step size
    return alpha, x_k1, f_k1


def steepest_dir(args, **kwargs):
    """ Function for calculating the steepest descent search direction

        Inputs
        args   : Dictionary of arguments, requires:
                 f   : Class instance for cost function with grad() method
                       for calculating the gradient at a point
                 x_k : Point at which to calculate the gradient
        kwargs : Optional keyword args:
                 df_k  : Holds df_k if it is already known

        Outputs
        Steepest descent search direction -f(x_k)
    """
    # return steepest descent direction at x_k
    if 'df_k' in kwargs.keys():
        return -kwargs['df_k']
    return -args['f'].grad(args['x_k'])


def newton_dir(args, **kwargs):
    """ Function for calculating the Newton's method search direction

        Inputs
        args   : Dictionary of arguments, requires:
                 f   : Class instance for cost function with grad() and
                       hess() methods for calculating the gradient and
                       the Hessian at a point
                 x_k : Point at which to calculate the gradient and hessian
        kwargs : Optional keyword args:
                 df_k  : Holds df_k if it is already known

        Outputs
        Newton's method search direction -(d^2f(x_k))^{-1}(df(x_k)) as the
        solution to the linear system -d^2f(x_k)x=df(x_k)
    """
    # return Newton's method direction at x_k
    if 'df_k' in kwargs.keys():
        return -np.linalg.solve(args['f'].hess(args['x_k']),kwargs['df_k'])
    return -np.linalg.solve(args['f'].hess(args['x_k']),args['f'].grad(args['x_k']))


def FR_dir(args, **kwargs):
    """ Function for finding the Fletcher-Reeves nonlinear CG direction

        Inputs
        args   : Dictionary of arguments, requires:
                 x_kf  : Class instance for cost function with grad() method
                       for calculating the gradient at a point
                 x_k : Point at which to calculate the gradient
        kwargs : Optional keyword args:
                 df_k  : Holds df_k if it is already known
    """
    # populate or compute grad(x_k) at current iteration x_k)
    if 'df_k' in kwargs.keys():
        df_k = kwargs['df_k']
    else:
        df_k = args['f'].grad(args['x_k'])
    # The previous search direction and the norm of the previous gradient
    # are needed for the next iteration. Those will not be defined the first
    # time the function is invoked, so return p_k = -grad(x_k) and store
    # p_k and ||grad(x_k)|| to be used for the next iteration.
    if not 'p_k' in args.keys():
        g_k = df_k
        p_k = -g_k
        args['p_k'] = p_k
        args['normg'] = np.linalg.norm(g_k)
        return p_k
    # find Fletcher-Reeves nonlinear CG direction
    g_k = df_k
    normg = np.linalg.norm(g_k)
    beta_k = (normg / args['normg'])**2
    p_k = -g_k + beta_k*args['p_k']
    # return p_k = -grad(x_k) if calculated p_k is not a descent direction
    if (p_k @ g_k) >= 0:
        p_k = -df_k
        print('*** restart at iteration %d' % kwargs['niter'])
    # store direction and norm of gradient for next iteration
    args['p_k'] = p_k
    args['normg'] = normg
    # return new direction
    return p_k

def FR_dir_dot(args, **kwargs):
    """ Function for finding the Fletcher-Reeves nonlinear CG direction
        using g@g for \|g\|^2 instead of norm(g)^2

        Inputs
        args   : Dictionary of argumentsf, requires:
                 x_kf  : Class instance for cost function with grad() method
                       for calculating the gradient at a point
                 x_k : Point at which to calculate the gradient
        kwargs : Optional keyword args:
                 df_k  : Holds df_k if it is already known
    """
    # populate or compute grad(x_k) at current iteration x_k)
    if 'df_k' in kwargs.keys():
        df_k = kwargs['df_k']
    else:
        df_k = args['f'].grad(args['x_k'])
    # The previous search direction and the norm of the previous gradient
    # are needed for the next iteration. Those will not be defined the first
    # time the function is invoked, so return p_k = -grad(x_k) and store
    # p_k and ||grad(x_k)|| to be used for the next iteration.
    if not 'p_k' in args.keys():
        g_k = df_k
        p_k = -g_k
        args['p_k'] = p_k
        args['g_dot_g'] = g_k @ g_k
        return p_k
    # find Fletcher-Reeves nonlinear CG direction
    g_k = df_k
    g_dot_g = g_k @ g_k
    beta_k = g_dot_g / args['g_dot_g']
    p_k = -g_k + beta_k*args['p_k']
    # return p_k = -grad(x_k) if calculated p_k is not a descent direction
    if (p_k @ g_k) >= 0:
        p_k = -df_k
        print('*** restart at iteration %d' % kwargs['niter'])
    # store direction and norm of gradient for next iteration
    args['p_k'] = p_k
    args['g_dot_g'] = g_dot_g
    # return new direction
    return p_k


def graditer(x_k, f, stepdir, stepdir_args, stepsize, stepsize_args, \
        eps=1.0e-5, maxiter=1.0e+6):
    """ graditer(): Iterative line search optimization algorithm

        This solves an optimization problem where at each point one
        chooses a direction and a step size. It can use any function
        for choosing a step direction and any function for choosing a
        step size at a given point x_k.

        Inputs
        x_k           : Starting point
        f             : Class instance for cost function. Requires methods
                        f.eval(x) returning f(x) and f.grad(x) returning
                        grad f(x)
        stepdir       : Function for computing a step direction at the current
                        iterate x_k
        stepdir_args  : Arguments required for step direction function
        stepsize      : Function for returning step size alpha at the current
                        iterate x_k and direction p_k
        stepsize_args : Arguments required for step size function
        eps           : Stopping criterion - stop if norm of gradient is
                        less than eps (optional - default 1.0e-5)
        maxiter       : Stopping criterion - stop if number of iterations
                        reaches maxiter (optional - default 1.0e+6)

        Outputs
        iters         : 2d array of iterates {x_k}
        f_vals        : Array of f values {f(x_k)}
        norm_df_vals  : Array of ||df|| values {||df(x_k)||}
    """
    # initialize niter (# iterations) and iters list (stores iterate data)
    # and store initial iterate, f_k, and |df_k| vals
    niter = 0
    iters = np.array([x_k])
    f_k = f.eval(x_k)
    f_vals = np.array([f_k])
    df_k = f.grad(x_k)
    norm_df_k = np.linalg.norm(df_k)
    norm_df_vals = np.array([norm_df_k])

    # begin loop
    while niter < maxiter:
        stepdir_args['x_k'] = x_k
        p_k = stepdir(stepdir_args, df_k=df_k, niter=niter)
        # set x_k and direction p_k in stepsize_args and get next step size
        stepsize_args['x_k'] = x_k
        stepsize_args['f_k'] = f_k
        stepsize_args['p_k'] = p_k
        alpha, _x_k1, _f_k1 = stepsize(stepsize_args, df_k=df_k)
        # calculate next iterate
        if (_x_k1 != x_k).all():
            x_k = _x_k1
        else:
            x_k = x_k + alpha * p_k
        # update values for next loop and store current state
        if _f_k1 != f_k:
            f_k = _f_k1
        else:
            f_k = f.eval(x_k)
        df_k = f.grad(x_k)
        norm_df_k = np.linalg.norm(df_k)
        iters = np.append(iters, [x_k], axis=0)
        f_vals = np.append(f_vals, f_k)
        norm_df_vals = np.append(norm_df_vals, norm_df_k)
        # increment number of iterations
        niter += 1
        # stop if |df(x_k)| < eps
        if norm_df_k < eps:
            break
    # return results
    return iters,f_vals,norm_df_vals

def cgiter(x_k, A, b, eps=1.0e-5, maxiters=1.0e+4):
    """ Linear CG function for solving Ax=b

        Required Inputs
        x_k      : Starting point
        A        : Matrix A in Ax=b
        b        : Solution b in Ax=b

        Optional Inputs
        eps      : Stop when ||Ax-b|| < eps (default 1.0e-5)
        maxiters : Stop if iterations exceed maxiter (default 1.0e+4)

        Outputs
        iters    : Array of iterates - last one is the solution
        r_k_vals : Array of residuals for each iterate
    """

    # initialize niter (# iterations), iters (iterats), r_k_vals (residuals)
    niter = 0
    iters = np.array([x_k])
    r_k = A@x_k - b
    r_k_vals = np.array([r_k])
    # calculate first residual norm and first CG direction
    norm_r_k = np.linalg.norm(r_k)
    p_k = -r_k
    # begin CG loop
    while (norm_r_k > eps) and (niter < maxiters):
        # generate next iterate x_k and residual r_k
        Ap_k = A@p_k
        alpha_k = (norm_r_k**2)/(p_k @ Ap_k)
        x_k1 = x_k + alpha_k * p_k
        r_k1 = r_k + alpha_k * Ap_k
        # save x_k and r_k
        iters = np.append(iters, [x_k1], axis=0)
        r_k_vals = np.append(r_k_vals, r_k1)
        # get next search direction p_k
        norm_r_k1 = np.linalg.norm(r_k1)
        beta_k1 = (norm_r_k1**2)/(norm_r_k**2)
        p_k1 = -r_k1 + beta_k1 * p_k
        # store new iterate, direction, residual, residual norm for next loop
        x_k = x_k1
        p_k = p_k1
        r_k = r_k1
        norm_r_k = norm_r_k1
        niter += 1

    # return iterates and residuals
    return iters,r_k_vals
