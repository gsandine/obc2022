#!/usr/bin/env python

# I used this code to generate the plots. It is a work in progress
# not prepared for sharing but shows what I did to generate the plots.

# import numerical libraries
import numpy as np

# import plotting libraries and set parameters
import matplotlib
# requires LaTeX with type1ec.sty, and dvipng
# Ubuntu packages: texlive-latex-extra cm-super-minimal dvipng
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['font.size']=14
import matplotlib.pyplot as plt

def plot_f_vals(res, show):
    for A in res.keys():
        # steepest descent cost function
        plt.clf()
        plt.plot(res[A]['s']['f_vals'], linewidth='2')
        plt.yscale('log')
        if A==1:
            plt.ylim((1.0e-13,1.0e+1))
        else:
            plt.ylim((1.e-12,1.e+2))
        plt.xlabel(r'$k$')
        plt.ylabel(r'$\log f_k$')
        plt.title(r'$\textrm{Cost Function Evolution for Steepest Descent }(A=%d)$' % A)
        plt.tight_layout()
        if show:
            plt.show()
        else:
            plt.savefig('f_vals-steepest-A=%03d.png' % A)

        # Newton's method cost function
        plt.clf()    
        plt.plot(res[A]['n']['f_vals'], lw=2)
        plt.yscale('log')
        if A==1:
            plt.ylim((1.0e-13,1.0e+1))
        else:
            plt.xlim((-1,22))
            plt.ylim((1.e-12,1.e+2))
        plt.xlabel(r'$k$')
        plt.ylabel(r'$\log f_k$')
        plt.title(r"$\textrm{Cost Function Evolution for Newton's Method }(A=%d)$" % A)
        plt.tight_layout()
        if show:
            plt.show()
        else:
            plt.savefig('f_vals-newton-A=%03d.png' % A)

def plot_df_vals(res, show):
    for A in res.keys():
        # full plot - norm of gradient for S
        plt.clf()
        plt.plot(res[A]['s']['norm_df_vals'], lw=1, c='b', label=r'$\log\|\nabla f_k\|$')
        stop = np.ones(len(res[A]['s']['norm_df_vals']))*1.0e-3
        plt.plot(stop, lw=1.25, c='r', alpha=0.5, ls='--', label=r'$\textrm{stop}$')
        plt.yscale('log')
        if A==1:
            plt.ylim((1.e-6,10.))
        else:
            plt.ylim((1.e-5,1.e+3))
        plt.xlabel(r'$k$')
        plt.ylabel(r'$\log\|\nabla f_k\|$')
        plt.legend()
        plt.title(r'$\textrm{Norm of Gradient for Steepest Descent }(A=%d)$' % A)
        plt.tight_layout()
        if show:
            plt.show()
        else:
            plt.savefig('norm_df_vals-steepest-A=%03d.png' % A)

        # full plot - norm of gradient for N
        plt.clf()
        plt.plot(res[A]['n']['norm_df_vals'], lw=2, c='b', label=r'$\log\|\nabla f_k\|$')
        stop = np.ones(len(res[A]['n']['norm_df_vals']))*1.0e-3
        plt.plot(stop, lw=1.25, c='r', alpha=0.5, ls='--', label=r'$\textrm{stop}$')
        plt.yscale('log')
        if A==1:
            plt.ylim((1.e-6,10.))
        else:
            plt.xlim((-1,22))
            plt.ylim((1.e-5,1.e+3))
        plt.xlabel(r'$k$')
        plt.ylabel(r'$\log\|\nabla f_k\|$')
        plt.legend()
        plt.title(r"$\textrm{Norm of Gradient for Newton's Method }(A=%d)$" % A)
        plt.tight_layout()
        if show:
            plt.show()
        else:
            plt.savefig('norm_df_vals-newton-A=%03d.png' % A)

        if A == 100:
            # 60, 196, ~0.0737, ~0.0609
            # initial iterates - norm of gradient for S, only needed for A=100
            plt.clf()
            stop = 201
            plt.plot(res[A]['s']['norm_df_vals'][:stop], lw=1, c='b', label=r'$\log\|\nabla f_k\|$')
            stop = np.ones(len(res[A]['s']['norm_df_vals'][:stop]))*1.0e-3
            plt.plot(stop, lw=1.25, c='r', alpha=0.5, ls='--', label=r'$\textrm{stop}$')
            plt.yscale('log')
            plt.ylim((0.5*10**(-3),1.e+3))
            plt.xlabel(r'$k$')
            plt.ylabel(r'$\log\|\nabla f_k\|$')
            plt.legend()
            plt.title(r'$\textrm{Norm of Gradient for Steepest Descent }(A=%d)$' % A)
            plt.tight_layout()
            if show:
                plt.show()
            else:
                plt.savefig('norm_df_vals-steepest-A=%03d-start.png' % A)

        if A == 100:
            # final iterates - norm of gradient for $S, only needed for A=100
            plt.clf()
            start = -201
            niters = len(res[A]['s']['iters'])-1
            plt.plot(res[A]['s']['norm_df_vals'][start:], lw=1, c='b', label=r'$\log\|\nabla f_k\|$')
            stop = np.ones(len(res[A]['s']['norm_df_vals'][start:]))*1.0e-3
            plt.plot(stop, lw=1.25, c='r', alpha=0.5, ls='--', label=r'$\textrm{stop}$')
            plt.yscale('log')
            k = [r'$\textrm{%d}$'%n for n in np.arange(niters+(start+1),niters+1,25)]
            plt.xticks(range(0,-start,25),k)
            plt.ylim((0.5*10**(-3),1.e-2))
            plt.xlabel(r'$k$')
            plt.ylabel(r'$\log\|\nabla f_k\|$')
            plt.legend()
            plt.title(r'$\textrm{Norm of Gradient for Steepest Descent }(A=%d)$' % A)
            plt.tight_layout()
            if show:
                plt.show()
            else:
                plt.savefig('norm_df_vals-steepest-A=%03d-end.png' % A)

def plot_x_path_z(res, show, zoom=0):
    # ran this manually adjusting boundaries for steepest descent with
    # zoom=-151 to show the end and zoom=30 to show the beginning
    for A in res.keys():
        for m in 'n','s':
            if zoom <= 0:
                iters = res[A][m]['iters'][zoom:]
                flstr = 'Last'; flnum = -zoom-1
            else:
                iters = res[A][m]['iters'][1:zoom+1]
                flstr = 'First'; flnum = zoom
            plt.clf()
            s = 5.
            if m == 's':
                s=0.5; s=5.
            #plt.scatter(*zip(*iters),s=s,c=np.arange(len(iters))/(len(iters)-1),cmap='seismic')
            dotcolor = 'k'
            if zoom < 0:
                dotcolor = 'r'
            elif zoom > 0:
                dotcolor = 'b'
            plt.scatter([1.],[1.],marker='+',color=dotcolor,s=20.)
            xmax=np.max(iters[:,0])+1.0e-5
            xmin=np.min(iters[:,0])-1.0e-5
            ymax=np.max(iters[:,1])+1.0e-5
            ymin=np.min(iters[:,1])-1.0e-5
            if zoom == 0:
                xmax = np.round(np.max([xmax+0.1,0.1]),1)
                xmin = np.round(np.min([xmin-0.1,-0.1]),1)
                ymax = np.round(np.max([ymax+0.1,0.1]),1)
                ymin = np.round(np.min([ymin-0.1,-0.1]),1)
                plt.scatter([iters[0][0]],[iters[0][1]],color='k',s=10.)
            dx=5.*(xmax-xmin)/1000.
            dy=5.*(ymax-ymin)/1000.
            plt.xlim((xmin,xmax))
            plt.ylim((ymin,ymax))
            plt.xlabel(r'$x_1$')
            plt.ylabel(r'$x_2$')
            method = "Newton's Method"; smethod = 'newton'
            if m == 's':
                method = '%s %d Steepest Descent' % (flstr,flnum); smethod = 'steepest'
            plt.title(r'$\textrm{%s Iterations }(A=%d)$' % (method,A))
            plt.plot(np.arange(xmin,xmax+0.9e-4*dx,dx),np.zeros(len(np.arange(xmin,xmax+0.9e-4*dx,dx))),c='k',lw=1.5,alpha=0.7)
            plt.plot(np.zeros(len(np.arange(ymin,ymax+0.9e-4*dy,dy))),np.arange(ymin,ymax+0.9e-4*dy,dy),c='k',lw=1.5,alpha=0.7)
            plt.grid()
            cplot(xmin,xmax,ymin,ymax,dx,dy,A)
            plt.tight_layout()
            if show:
                plt.show()
            else:
                plt.savefig('x_path-%s-A=%03d-%s%03d.png' % (smethod,A,flstr,flnum))

def plot_x_path(res, show, zoom=0):
    for A in res.keys():
        for m in 'n','s':
            iters = res[A][m]['iters']
            plt.clf()
            s = 5.
            if m == 's': s=0.5
            plt.scatter(*zip(*iters),s=s,c=np.arange(len(iters))/(len(iters)-1),cmap='seismic')
            plt.scatter([1.],[1.],marker='+',color='r',s=30.)
            plt.scatter([iters[0][0]],[iters[0][1]],color='k',s=20.)
            xmax=np.round(np.max([np.max(res[A][m]['iters'][:,0]),0.])+0.1,1)
            xmin=np.round(np.min([np.min(res[A][m]['iters'][:,0]),0.])-0.1,1)
            ymax=np.round(np.max([np.max(res[A][m]['iters'][:,1]),0.])+0.1,1)
            ymin=np.round(np.min([np.min(res[A][m]['iters'][:,1]),0.])-0.1,1)
            dx=0.005;dy=0.005
            plt.xlim((xmin,xmax))
            plt.ylim((ymin,ymax))
            plt.xlabel(r'$x_1$')
            plt.ylabel(r'$x_2$')
            method = "Newton's Method"; smethod = 'newton'
            if m == 's':
                method = 'Steepest Descent'; smethod = 'steepest'
            plt.title(r'$\textrm{%s Iterations }(A=%d)$' % (method,A))
            plt.plot(np.arange(xmin,xmax+0.9*dx,dx),np.zeros(len(np.arange(xmin,xmax+0.9*dx,dx))),c='k',lw=1.5,alpha=0.7)
            plt.plot(np.zeros(len(np.arange(ymin,ymax+0.9*dy,dy))),np.arange(ymin,ymax+0.9*dy,dy),c='k',lw=1.5,alpha=0.7)
            plt.grid()
            cplot(xmin,xmax,ymin,ymax,dx,dy,A)
            plt.tight_layout()
            if show:
                plt.show()
            else:
                plt.savefig('x_path-%s-A=%03d.png' % (smethod,A))

def plots(**kwargs):
    res = kwargs['res']
    show = False
    if 'show' in kwargs.keys():
        show = True

    if 'f_vals' in kwargs.keys():
        plot_f_vals(res, show)
    if 'df_vals' in kwargs.keys():
        plot_df_vals(res, show)
    if 'x_path' in kwargs.keys():
        plot_x_path(res, show)

if __name__ == '__main__':
    plots()

def cplot(xmin,xmax,ymin,ymax,dx,dy,A):
    from hw2 import rosen
    f=rosen(A=A)
    #xmax=2.; dx=0.005; ymax=xmax; dy=dx
    #x=np.arange(-xmax,xmax+dx,dx);y=np.arange(-ymax,ymax+dy,dy)
    x=np.arange(xmin,xmax+0.9*dx,dx);y=np.arange(ymin,ymax+0.9*dy,dy)
    z=np.zeros((y.shape[0],x.shape[0]))
    for yi in range(y.shape[0]):
        for xj in range(x.shape[0]):
            xy = np.array([x[xj],y[yi]])
            #xy = np.array([y[-(yi+1)],x[xj]])
            z[yi,xj] = f.eval(xy)
    #plt.contour(x,y,z,np.arange(0.1,100.1,10.),colors='b',alpha=0.5)
    #cmax = z.max()/(0.2*A); numc = 40.
    #plt.contour(x,y,z,np.arange(0.1,cmax,cmax/numc),colors='r',alpha=0.2)
    plt.contour(x,y,z,100,colors='k',alpha=0.1)
    #plt.show()
