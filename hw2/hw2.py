#!/usr/bin/env python

# import numerical libraries
import numpy as np

# append to path to import local libraries
import sys, os
sys.path.append(os.path.join(sys.path[0],'../lib'))
from mth564 import *

class rosen:
    """ Class for Rosenbrock test function with domain $^2:
        f(x) = A (x1 - x0^2)^2 + (1 - x0)^2

        Methods:
        eval(x) : returns f(x)
        grad(x) : returns the gradient df(x)
        hess(x) : returns the Hessian d2f(x)

        Attributes:
        f_evals     : number of f evaluations
        df_evals    : number of gradient evaluations
        d2f_evals   : number of Hessian evaluations
    """
    def eval(self, x):
        """ Class method that returns f(x)

            Inputs
            x    : Numpy 2d array at which to evaluate f

            Outputs
            f(x) : Value of the Rosenbrock function at x
        """
        # increment f_evals counter
        self.f_evals += 1
        # return f(x)
        return (self.A)*(x[1]-x[0]*x[0])**2+(1-x[0])**2
        #return (self.A)*np.power(x[1]-x[0]*x[0],2)+np.power(1-x[0],2)

    def grad(self, x):
        """ Class method that returns grad f(x)

            Inputs
            x     : Numpy 2d array at which to compute grad f

            Outputs
            df(x) : Gradient of the Rosenbrock function at x
        """
        # increment df_evals counter
        self.df_evals += 1
        # compute partial derivatives
        df_dx0 = -4*(self.A)*(x[1]-x[0]**2)*x[0]-2*(1-x[0])
        df_dx1 = 2*(self.A)*(x[1]-x[0]**2)
        # return df(x)
        return np.array([df_dx0,df_dx1])

    def hess(self, x):
        """ Class method that returns Hessian f(x)

            Inputs
            x      : Numpy 2d array at which to compute Hessian f

            Outputs
            d2f(x) : Hessian of the Rosenbrock function at x
        """
        # increment d2f_evals counter
        self.d2f_evals += 1
        # compute partial derivatives
        d2f_d2x1 = 12*(self.A)*(x[0])**2-4*(self.A)*x[1]+2
        d2f_d2x1x2 = -4*(self.A)*x[0]
        d2f_d2x2x1 = d2f_d2x1x2
        d2f_d2x2 = 2*(self.A)
        # return d2f(x)
        return np.array([[d2f_d2x1,d2f_d2x1x2],[d2f_d2x2x1,d2f_d2x2]])

    def __init__(self, A):
        """ Class initialization method - sets parameter for Rosenbrock
            function and initializes counters for the number of f, df,
            and d2f evaluations to 0

            Inputs
            A      : Parameter for Rosenbrock function A(x2-x1^2)^2+(1-x1)^2
        """
        # initialize parameter for Rosenbrock function
        self.A = A
        # initialize counters for the number of f, df, and d2f evaluations
        self.f_evals = 0
        self.df_evals = 0
        self.d2f_evals = 0

def hw2():
    # initialize x0
    x0 = np.array([-1.2,1.])

    # create dictionary for storing results
    res = {}

    # loop over A values for Rosenbrock function
    for A in 1., 100.:
        # initialize dictionaries for storing results for this A value
        res[A] = {}; s_res = {}; n_res = {}

        # initialize f with parameter A
        f = rosen(A)

        # initialize arguments for step direction function - it's the same for
        # this assignment for both methods (steepest descent and Newton's method)
        stepdir_args = {'f': f}

        # initialize arguments for step size functions - they're the same for this
        # assignment for both methods (steepest descent and Newton's method)
        stepsize_args = {'f': f, 'alpha': 1., 'rho': 0.5, 'c': 0.01}

        # invoke steepest descent algorithm with backtracking step size selection
        s_res['iters'],s_res['f_vals'],s_res['norm_df_vals'] = \
            graditer(x0, f, steepest_dir, stepdir_args, backtrack_stepsize, \
                stepsize_args, eps=1.0e-3)
        # store and reset function and gradient evaluation counters
        s_res['evals'] = (f.f_evals, f.df_evals)
        f.f_evals,f.df_evals = 0,0

        # invoke Newton's method algorithm with backtracking step size selection
        n_res['iters'],n_res['f_vals'],n_res['norm_df_vals'] = \
            graditer(x0, f, newton_dir, stepdir_args, backtrack_stepsize, \
                stepsize_args, eps=1.0e-3)
        # store function, gradient, and Hessian evaluation counters
        n_res['evals'] = (f.f_evals, f.df_evals, f.d2f_evals)

        # save results in res dictionary
        res[A] = {}; res[A]['s'] = s_res; res[A]['n'] = n_res

    # return results
    return res

if __name__ == '__main__':
    res = hw2()
